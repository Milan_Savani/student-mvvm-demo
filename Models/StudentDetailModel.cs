﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace student.Models
{
    public partial class StudentDetailModel : ObservableObject
    {
        [ObservableProperty]
        private int id;

        [ObservableProperty]
        private string name;

        [ObservableProperty]
        private string unit;

        [ObservableProperty]
        private string valueType;

        [ObservableProperty]
        private float factor;

        [ObservableProperty]
        private float offset;

        [ObservableProperty]
        private string byteOrder;



        public StudentDetailModel()
        {
            
        }

        public StudentDetailModel Clone()
        {
            return new StudentDetailModel
            {
                Name = $"{Name} clone",
                Id = Id,
            };
        }

        public StudentDetailModel UpdateFrom(StudentDetailModel otherStudent)
        {
            Id = otherStudent.Id;
            Name= otherStudent.Name;
            return this;
        }

        public bool ApplyFilter(int filter)
        {
            return Id==filter;
        }

        public override string ToString()
        {
            return Name;
        }
    }

}

using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using student.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace student.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StudentDetails : UserControl
    {
        
        public StudentDetailsViewModel ViewModel{ get; } = new StudentDetailsViewModel();

        public StudentDetails()
        {
            this.InitializeComponent();
            
        }

   

        private void StudentList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ViewModel.DetailsVisibility = Visibility.Visible;
            if (ViewModel.SelectedStudent != null)
            {
            
                ID.Text = ViewModel.SelectedStudent.Id.ToString();
                Name.Text = ViewModel.SelectedStudent.Name;
            }
            else
            {
                ID.Text = string.Empty;
                Name.Text = string.Empty;
                Description.Text = string.Empty;
            }
            


        }

        
    }
}

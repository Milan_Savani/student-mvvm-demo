﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using student.Models;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;

namespace student.ViewModels
{
    public partial class StudentDetailsViewModel : ObservableObject
    {


        [ObservableProperty]
        private int id;

        [ObservableProperty]
        private string name;

        [ObservableProperty]
        private string unit;

        [ObservableProperty]
        private string valueType;

        [ObservableProperty]
        private float factor;

        [ObservableProperty]
        private float offset;

        [ObservableProperty]
        private string byteOrder;




        [ObservableProperty]
        private StudentDetailModel selectedStudent;

        public ObservableCollection<StudentDetailModel> Students { get; }

        [ObservableProperty]
        private Visibility detailsVisibility;


        public StudentDetailsViewModel()
        {
            Students = new ObservableCollection<StudentDetailModel>();
            detailsVisibility = Visibility.Collapsed;


        }

        public void DetailView()
        {
            
        }


        [RelayCommand]
        public void OnAddStudent()
        {
            if (!string.IsNullOrWhiteSpace(Name))
            {
                Students.Add(new StudentDetailModel { Id = Id, Name = Name, ByteOrder = ByteOrder, Factor = Factor , Offset = Offset, Unit = Unit, ValueType = ValueType });
                ResetValues();
            }
        }

        public void ResetValues()
        {
            Name = string.Empty;
            Id = 0;
            Factor = 0;
            Offset = 0;
            Unit = string.Empty;
            ValueType = string.Empty;
            ByteOrder = string.Empty;


        }

        [RelayCommand]
        public void OnUpdateStudent()
        {


            if (SelectedStudent != null && !string.IsNullOrWhiteSpace(Name))
            {
                var i = Students.IndexOf(SelectedStudent);

                SelectedStudent.Name = Name;
                Students[i] = SelectedStudent;
                Name = string.Empty;

            }
        }

        [RelayCommand]
        public void OnDeleteStudent()
        {
            if (!string.IsNullOrWhiteSpace(Name))
            {
                var studentToRemove = Students.FirstOrDefault(s => s.Id == Id && s.Name == Name);
                if (studentToRemove != null)
                {
                    Students.Remove(studentToRemove);
                    SelectedStudent = null;
                    OnPropertyChanged(nameof(Students));
                }
            }else
            {
                if (SelectedStudent != null)
                {
                    Students.Remove(SelectedStudent);
                    Name = string.Empty;
                    OnPropertyChanged(nameof(Students));

                }
            }
        }

    }   

}

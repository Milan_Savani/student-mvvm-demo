﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace student.Models
{
    public partial class Subject : ObservableObject
    {
        [ObservableProperty]
        private string subjectName;

        [ObservableProperty]
        private int subjectId;

        [ObservableProperty]
        private int credit;
    }
}
